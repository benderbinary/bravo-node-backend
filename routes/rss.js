const express = require('express')
const router = express.Router()
const asyncHandler = require('express-async-handler')
const Parser = require('rss-parser')
const parser = new Parser()

const urlList = [
  'https://www.reddit.com/r/Coronavirus/.rss',
  'https://www.reddit.com/r/Health/.rss',
  'https://www.reddit.com/r/flu/.rss',
  'https://www.reddit.com/r/medicine/.rss',
  'https://www.reddit.com/r/MultipleSclerosis/.rss',
]

const getFeed = async () => {
  const rssList = urlList.map((url) => parser.parseURL(url))
  const feed = await Promise.all(rssList)

  return feed
}

/* GET coronavirus .rss page. */
router.get(
  '/',
  asyncHandler(async (req, res, next) => {
    const response = await getFeed()
    res.send(response)
  })
)

module.exports = router
